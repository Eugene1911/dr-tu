#-------------------------------------------------
#
# Project created by QtCreator 2018-12-20T11:40:54
#
#-------------------------------------------------

QT       += core gui
QT       += core serialport network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DR-TU
TEMPLATE = app


SOURCES += main.cpp\
        functions.cpp \
        messagehandler.cpp \
        port_rs485.cpp \
        commands.cpp \
        mainwindow.cpp \
    console.cpp \
    portudp.cpp

HEADERS  += mainwindow.h \
        messagehandler.h \
        functions.h \
        port_rs485.h \
    console.h \
    portudp.h

FORMS    += \
    mainwindow.ui \
    console.ui
