﻿#include "functions.h"

QBitArray bytesToBits(const QByteArray &bytes){
    QBitArray bits;
    bits.resize(bytes.size()*8);
    // Convert from QByteArray to QBitArray
    for(int i=0; i<bytes.count(); ++i)
        for(int b=0; b<8; ++b)
            bits.setBit(i*8+b, bytes.at(i)&(1<<(7-b)));
    return bits;
}

void formCheckMas(QVector<bool> *checkArr, QByteArray msg){
    for (int i=0; i<msg.size()-1; i++){
        quint8 byte;
        for (int k=1; k<8; k++){
            byte = 0;
            double mskD = pow(2, k);
            int mask = round(mskD);
            byte = msg[i] & mask;
            if (byte>0)
                checkArr->append(true);
            else checkArr->append(false);
        }
    }
}

quint8 checkSum(QByteArray &msg){
    QVector <bool> checkArr;
    formCheckMas(&checkArr, msg);
    quint8 crc = 254;
    quint8 byte;
    for (int i=0, k=0; i<msg.size()-1; i++, k+=7){
        byte = 0;
        for (int m=k; m<k+7; m++){
            double mskD;
            quint8 mask;
            if (checkArr[m] == true){
                mskD = pow(2, (m-k));
                mask = round(mskD);
                byte += mask;
            }
        }
        byte = byte << 1;
        crc ^= (byte ^ 144);
    }
    return crc;
}

uint8_t csAddBit(uint8_t initRegState, uint8_t newBit)
{
    uint8_t r6 = (initRegState >>6) & 0x1;
    initRegState  = initRegState <<1; // сдвиг влево текущей crc‚
    initRegState = initRegState ^ (r6<<4); // 4-ый бит‚
    initRegState |= ((newBit & 0x1) ^ r6); // 0-ой бит с новым‚
    return (initRegState & 0x7F);
}

uint8_t calcCS(const QByteArray &data)
{
    uint8_t curRegisterVal = 0x7F; // начальное значение 1111111
    for (int ind=0; ind<=data.size()-1; ind++)
    {
        uint8_t curByte = 0;
        if (ind<data.size()-1)
            curByte = data[ind];
        for (int jnd=0; jnd<7; jnd++)
        {
            curByte = curByte>>1;
            curRegisterVal = csAddBit(curRegisterVal, (curByte & 0x1));
        }
    }
    return (curRegisterVal << 1);
}
