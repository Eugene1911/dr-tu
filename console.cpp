#include "console.h"
#include "ui_console.h"

Console::Console(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Console)
{
    ui->setupUi(this);
}

Console::~Console()
{
    delete ui;
}

void Console::on_pbClear_clicked()
{
    ui->teSend->clear();
    ui->teReceive->clear();
    ui->leCSum->clear();
}

void Console::showRead(QByteArray msg)
{
    QString str1, str2;
    str1 = msg.toHex();
    for (int i=0;i<str1.size(); i+= 2)
        str2.append(QString("%1%2 ").arg(str1[i]).arg(str1[i+1]));
    QTime time;
    time = QTime::currentTime();
    QString strTime = time.toString("hh.mm.ss.zzz");
    str2.append(strTime);
    ui->teReceive->append(str2);
    quint8 cs = calcCS(msg);
    showCSum(cs);
}

void Console::showWrite(QByteArray msg)
{
    QString str1, str2;
    str1 = msg.toHex();
    for (int i=0;i<str1.size(); i+= 2)
        str2.append(QString("%1%2 ").arg(str1[i]).arg(str1[i+1]));
    QTime time;
    time = QTime::currentTime();
    QString strTime = time.toString("hh.mm.ss.zzz");
    str2.append(strTime);
    ui->teSend->append(str2);
}

void Console::showCSum(quint8 cs)
{
    QString str;
    str.setNum(cs, 16);
    ui->leCSum->setText(str);
}
