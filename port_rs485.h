﻿#ifndef PORT_RS485_H
#define PORT_RS485_H

#include <QSerialPort>
#include <QSettings>
#include <QObject>
#include "messagehandler.h"
#include <QApplication>

class Port_RS485 : public QObject
{
    Q_OBJECT
public:
    explicit Port_RS485(QObject *parent = 0);
    ~Port_RS485();
    bool checkPort();
    void openPort();
    void closePort();
private:
    const QString appPath;
    QSerialPort *serialPort;
    QSettings portSettings;
    QByteArray data;
    QString buffer;
    void serialPortSettings();
    bool serialPortInit();

private slots:
    void receive();
    void send(QByteArray);

signals:
    void sendMsgToHandler(QByteArray);
    void signIndicator(QByteArray);
};

#endif // PORT_RS485_H
