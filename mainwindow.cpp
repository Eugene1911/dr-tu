#include "mainwindow.h"
#include "ui_mainwindow.h"

const quint16 waitTimerDelay = 900;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    green("background-color: green"),
    gray("background-color: WhiteSmoke"),
    red("background-color: red"),
    amountAvarK1(0),
    amountAvarK2(0)
{
    ui->setupUi(this);

    //настройка отображения выбора каналов
    ui->gbChannel->setVisible(false);
    connect(ui->rbWEB, SIGNAL(toggled(bool)), ui->gbChannel, SLOT(setVisible(bool)));

    //таймер для отправки очереди сообщений
    queueTimer = new QTimer(this);
    queueTimer->start(500);
    connect(queueTimer, SIGNAL(timeout()), this, SLOT(sendQueue()));

    //таймер для отправки запросов ТС
    reqTimer = new QTimer(this);
    connect(reqTimer, SIGNAL(timeout()), this, SLOT(requestTechState()));

    begReqTimer = new QTimer(this);
    connect(begReqTimer, SIGNAL(timeout()), this, SLOT(beginRequest()));

    //таймер ожидание ответов ТС
    waitTimerDR1 = new QTimer(this);
    connect(waitTimerDR1, SIGNAL(timeout()), this, SLOT(controlConnectDR1()));
    waitTimerDR2 = new QTimer(this);
    connect(waitTimerDR2, SIGNAL(timeout()), this, SLOT(controlConnectDR2()));

    myMH = new MessageHandler(this);
    //обработка входящих сообщений
    connect(myMH, SIGNAL(signMsg(short int, QByteArray)), this, SLOT(readMsg(short int, QByteArray)));

    //инициализация и коннекты сигналов для класса Rs485
    serialPort = new Port_RS485(this);
    connect(this, SIGNAL(signSendToRs485(QByteArray)), serialPort, SLOT(send(QByteArray)));
    connect(serialPort, SIGNAL(sendMsgToHandler(QByteArray)), myMH, SLOT(getMsg(QByteArray)));
    connect(serialPort, SIGNAL(sendRemovedData(QByteArray)), this, SLOT(rememberRemovedData(QByteArray)));

    //инициализация и коннекты сигналов для класса UDP
    udpSockets = new PortUdp();
    connect(this, SIGNAL(signSendToUDP(QByteArray)), udpSockets, SLOT(send(QByteArray)));
    connect(udpSockets, SIGNAL(sendMsgToHandler(QByteArray)), myMH, SLOT(getMsg(QByteArray)));
    connect(udpSockets, SIGNAL(sendRemovedData(QByteArray)), this, SLOT(rememberRemovedData(QByteArray)));

    //коннект сигналов для выбора интерфейса
    connect(ui->rbRS485, SIGNAL(clicked()), this, SLOT(choicePort()));
    connect(ui->rbWEB, SIGNAL(clicked()), this, SLOT(choicePort()));
    choicePort();

    //коннект сигналов для отправки в консоль
    console = new Console();
    connect( ui->action_console, SIGNAL(triggered()), console, SLOT(show()));
    connect(this, SIGNAL(sendToConsoleWrite(QByteArray)), console, SLOT(showWrite(QByteArray)));
    connect(this, SIGNAL(sendToConsoleRead(QByteArray)), console, SLOT(showRead(QByteArray)));

    //таймер для отображения конца проверок
    endTimer = new QTimer(this);
    connect(endTimer, SIGNAL(timeout()), this, SLOT(endForm()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::choicePort()
{
    if (ui->rbRS485->isChecked()){
        udpSockets->close();
        serialPort->openPort();
    }
    else if (ui->rbWEB->isChecked()){
        serialPort->closePort();
        udpSockets->open();
    }
}

void MainWindow::on_pbBeginControl_clicked()
{
    ui->K1amountAvar->setValue(0);
    ui->K2amountAvar->setValue(0);
    amountAvarK1 = 0;
    amountAvarK2 = 0;
    QByteArray msg = QByteArray::fromHex("092a144200000000002c092A14E6201A0000009e092a1440000000000074092a14e4201a000000c6");
    queueData.append(msg);
    //sendQueue();
    begReqTimer->start(1000);
}

void MainWindow::beginRequest()
{
    begReqTimer->stop();
    reqTimer->start(1000);
}

void MainWindow::on_pbEndControl_clicked()
{
    QByteArray msg = QByteArray::
    fromHex("092a144200000000002c092a14620000000000f0092a1440000000000074092a14600000000000a8092A14E6201A0000009e092A14E4201A000000c6");
    queueData.append(msg);
    //sendQueue();
    reqTimer->stop();
    ui->K1amountAvar->setValue(0);
    amountAvarK1 = 0;
    ui->K2amountAvar->setValue(0);
    amountAvarK2 = 0;

    ui->K1avar->setStyleSheet(gray);
    ui->K1con->setStyleSheet(gray);
    ui->K1norm->setStyleSheet(gray);
    ui->K1rad->setStyleSheet(gray);
    ui->K1_KSV->setStyleSheet(gray);
    ui->K1_P->setStyleSheet(gray);

    ui->K2avar->setStyleSheet(gray);
    ui->K2con->setStyleSheet(gray);
    ui->K2norm->setStyleSheet(gray);
    ui->K2rad->setStyleSheet(gray);
    ui->K2_KSV->setStyleSheet(gray);
    ui->K2_P->setStyleSheet(gray);
    endTimer->start(1000);
}

void MainWindow::endForm()
{
    endTimer->stop();
    ui->K1avar->setStyleSheet(gray);
    ui->K1con->setStyleSheet(gray);
    ui->K1norm->setStyleSheet(gray);
    ui->K1rad->setStyleSheet(gray);
    ui->K1_KSV->setStyleSheet(gray);
    ui->K1_P->setStyleSheet(gray);

    ui->K2avar->setStyleSheet(gray);
    ui->K2con->setStyleSheet(gray);
    ui->K2norm->setStyleSheet(gray);
    ui->K2rad->setStyleSheet(gray);
    ui->K2_KSV->setStyleSheet(gray);
    ui->K2_P->setStyleSheet(gray);
}

void MainWindow::on_RadOff_clicked()
{
    QByteArray msg = QByteArray::fromHex("092a144200000000002c092A14E6201A0000009e092a1440000000000074092A14E4201A000000c6");
    queueData.append(msg);
    //sendQueue();
}

void MainWindow::on_K1on_clicked()
{
    QByteArray msg = QByteArray::
    fromHex("092a1440000000000074092a14600000000000a8092a14660000000000dc092a144a00000000003a092A14E6200A0000007c");
    queueData.append(msg);
    //sendQueue();
}

void MainWindow::on_K2on_clicked()
{
    QByteArray msg = QByteArray::
    fromHex("092a144200000000002c092a14620000000000f0092a1464000000000084092a1448000000000062092A14E4200A00000024");
    queueData.append(msg);
    //sendQueue();
}

void MainWindow::send(QByteArray msg)
{
    if (ui->rbRS485->isChecked()){
        if (serialPort->checkPort()){
            emit signSendToRs485(msg);
            emit sendToConsoleWrite(msg);
        }
    }
    else if (ui->rbWEB->isChecked()){
        if (udpSockets->checkSockets()){
            emit signSendToUDP(msg);
            emit sendToConsoleWrite(msg);
        }
    }
}

void MainWindow::requestTechState()
{
    if (queueData.size()>0)//если в буфере есть сообщения, то жду когда он освободится
        return;
    QByteArray msg = QByteArray::
    fromHex("09 2a 14 82 00 00 00 00 00 e4 09 2a 14 80 00 00 00 00 00 bc");
    queueData.append(msg);
    //sendQueue();
}

void MainWindow::on_ChKKButton_clicked()
{
    QByteArray msg1 = QByteArray::
    fromHex("09 2a 14 02 00 00 00 00 00 00");
    QByteArray msg2 = QByteArray::
    fromHex("09 2a 14 00 00 00 00 00 00 00");
    msg1[4] = (ui->SetChKK->value())<<1;
    msg1[9] = calcCS(msg1);
    msg2[4] = (ui->SetChKK->value())<<1;
    msg2[9] = calcCS(msg2);
    queueData.append(msg1);
    queueData.append(msg2);
    //sendQueue();
}
