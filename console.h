#ifndef CONSOLE_H
#define CONSOLE_H

#include <QWidget>
#include <functions.h>
#include <QTime>

namespace Ui {
class Console;
}

class Console : public QWidget
{
    Q_OBJECT

public:
    explicit Console(QWidget *parent = 0);
    ~Console();

public slots:
    void showWrite(QByteArray);
    void showRead(QByteArray);

private slots:
    void on_pbClear_clicked();

private:
    Ui::Console *ui;
    void showCSum(quint8);
};

#endif // CONSOLE_H
