﻿#include "messagehandler.h"

#include <QString>

MessageHandler::MessageHandler(QObject *parent): QObject (parent){
    reset();
}

MessageHandler::~MessageHandler(){

}

bool MessageHandler::checkMsg(QByteArray &data){
    //проверка правильности входящего сообщения
    bool trueMsg, trueSrc, trueType, trueSize;
    if (data[0] == 61)
        trueSrc = true;
    else trueSrc = false;
    if (data[1] == 44)
        trueType = true;
    else trueType = false;
    if (data[2] == 20)
        trueSize = true;
    else trueSize = false;
    if (data.size() == 10)
        trueSize = true;
    else trueSize = false;
    if ((trueSrc==true) && (trueType==true) && (trueSize==true))
        trueMsg = true;
    else trueMsg=false;
    return trueMsg;
}

void MessageHandler::getMsgKU(QByteArray ku){
    QBitArray bitArray = bytesToBits(ku);
    msgKU = bitArray[3*8]*4+bitArray[3*8+1]*2+bitArray[3*8+2]*1;
//    qDebug() << "Getting message KU " << msgKU;
}

void MessageHandler::reset(){
    msgKU = 10;
}

void MessageHandler::getMsg(QByteArray data){
    qDebug() << "My data is " << data.toHex();
    if (checkMsg(data)){
        getMsgKU(data);
        emit signMsg(msgKU, data);
        reset();
    } else{
        qDebug() << "Wrong message!";
        emit signMsg(0, data);
    }
}
