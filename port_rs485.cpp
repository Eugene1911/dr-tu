#include "port_rs485.h"
#include "QString"
#include "QDebug"

Port_RS485::Port_RS485(QObject *parent): QObject (parent),
    appPath( qApp->applicationDirPath() + "/"),
    portSettings( appPath + "RS485_settings.conf", QSettings::IniFormat)
{
    buffer = "";
    serialPort = new QSerialPort(this);
}

Port_RS485::~Port_RS485(){
    if (checkPort())
        serialPort->close();
    delete serialPort;
}

bool Port_RS485::checkPort(){
return serialPort->isOpen();
}

bool Port_RS485::serialPortInit(){
    serialPort->setPortName(portSettings.value("RS485/PortName").toString());
    qDebug() << "portName" << portSettings.value("RS485/PortName").toString();
    bool ok;
    bool portOpen = serialPort->open(QIODevice::ReadWrite);
//    qDebug () << "Port open " << portOpen;
    if (portOpen){
        //    qDebug () << "open comand";
            serialPort->setDataBits(QSerialPort::DataBits(portSettings.value("RS485/DataBits", 0).toInt(&ok)));
        //    qDebug () << "dataBits comand";
            serialPort->setParity(QSerialPort::Parity(portSettings.value("RS485/Parity", 0).toInt(&ok)));
        //    qDebug () << "parity comand";
            serialPort->setStopBits(QSerialPort::StopBits(portSettings.value("RS485/StopBits", 0).toInt(&ok)));
        //    qDebug () << "stopBits comand";
            serialPort->setFlowControl(QSerialPort::FlowControl(portSettings.value("RS485/FlowControl", 0).toInt(&ok)));
        //    qDebug () << "flowControl comand";
            serialPort->setBaudRate(portSettings.value("RS485/BaudRate", 0).toInt(&ok));
        //    qDebug () << "baudRate comand";
        //    connect (serialPort, SIGNAL(readyRead()), this, SLOT(receive()));
    }
    return portOpen;
}

void Port_RS485::receive(){
    data.append(serialPort->readAll());
    if (data.size()<10)
        return;
    QByteArray msg;
    for (int i = 0; i<10; i++)
        msg.append(data.at(i));
//    qDebug() << msg.toHex();
    data.remove(0,10);
    //проверка на заголовок
    while ((msg[0] & 1)==0 and (data.size()>0)){
        msg.remove(0,1);
        msg.append(data[0]);
        data.remove(0,1);
    }
    if (((msg[0]&1)==0) or (msg.size()<10))
        return;
    //проверка на новый заголовок
    for (int i=1; i<10; i++){
        if ((msg[i]&1)==1){
            msg.remove(0, i);
            receive();
        }
    }
    quint8 crc = calcCS(msg);
    //проверка контрольной суммы
//    if (msg[9]==calcCS(msg))
        emit sendMsgToHandler(msg);
}

void Port_RS485::openPort(){
    bool initialization;
    qDebug () << "Pre init ";
    initialization = serialPortInit();
    connect (serialPort, SIGNAL(readyRead()), this, SLOT(receive()));
}

void Port_RS485::closePort(){
    disconnect (serialPort, SIGNAL(readyRead()), this, SLOT(receive()));
    if (checkPort())
        serialPort->close();
}

void Port_RS485::send(QByteArray msg){
    serialPort->write(msg);
    //serialPort->waitForBytesWritten(50);
}
