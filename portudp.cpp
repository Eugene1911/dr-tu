#include "portudp.h"
#include <QDebug>

PortUdp::PortUdp(QObject *parent) :
    QObject(parent),
    appPath( qApp->applicationDirPath() + "/"),
    udpSettings( appPath + "UDP_settings.conf", QSettings::IniFormat)
{
    init();
}

PortUdp::~PortUdp()
{

}

void PortUdp::init()
{
    qDebug() << "udp init";
    dr1_port = udpSettings.value("settings/dr1_port", 0).toInt();
    dr2_port = udpSettings.value("settings/dr2_port", 0).toInt();
    ksrp_port = udpSettings.value("settings/ksrp_port", 0).toInt();
    dr1_ip = udpSettings.value("settings/dr1_ip").toString();
    dr2_ip = udpSettings.value("settings/dr2_ip").toString();
    ksrp_ip = udpSettings.value("settings/ksrp_ip").toString();
    socketDr1 = new QUdpSocket(this);
    socketDr2 = new QUdpSocket(this);
    socketKSRP = new QUdpSocket(this);
}

bool PortUdp::checkSockets()
{
    bool check = true;
    if (!socketDr1->isOpen())
        check = false;
    if (!socketDr2->isOpen())
        check = false;
    return check;
}

void PortUdp::open()
{
    qDebug() << "udp open";
    //открытие udp портов
    QHostAddress adrDr1, adrDr2, adrKSRP;
    adrDr1 = dr1_ip; adrDr2 = dr2_ip; adrKSRP = ksrp_ip;
    socketDr1->connectToHost(adrDr1, dr1_port);
    socketDr2->connectToHost(adrDr1, dr1_port);
    socketKSRP->bind(adrKSRP, ksrp_port);
    socketKSRP->open(QIODevice::ReadOnly);
    connect(socketKSRP, SIGNAL(readyRead()), this, SLOT(read()));
}

void PortUdp::close()
{
    socketDr1->close();
    socketDr2->close();
    socketKSRP->close();
}

void PortUdp::read()
{
    qDebug() << "udp read";
    QByteArray datagram, removedData;

    while (socketKSRP->hasPendingDatagrams()){
        datagram.resize(socketKSRP->pendingDatagramSize());
        QHostAddress sender;
        quint16 senderPort;

        socketKSRP->readDatagram(datagram.data(), datagram.size(), &sender, &senderPort);
    }
    recMas.append(datagram);

    //добавление данных в msg
    while (msg.size()<10){
        if (recMas.size()>0){
            msg.append(recMas[0]);
            recMas.remove(0, 1);
        }
        else break;//если в recMas ничего нет, выходим из цикла
    }

    if (msg.size()<10)
        return;

    //проверка на заголовок
    while ((msg[0] & 1)==0){
        msg.remove(0,1);
        if (recMas.size()>0){
            msg.append(recMas[0]);
            recMas.remove(0,1);
        }
        else if (removedData.size()>0) {
            emit sendRemovedData(removedData);
        }
    }

    if (removedData.size()>0)
        emit sendRemovedData(removedData);
    //проверка на размер сообщения
    if (((msg[0]&1)==0) or (msg.size()<10))
        return;

    //проверка на новый заголовок
    for (int i=1; i<10; i++){
            if ((msg[i]&1)==1){
                removedData.resize(i+1);
                qCopy(msg.begin(), msg.begin()+i+1, removedData.begin());
                emit sendRemovedData(removedData);
                msg.remove(0, i+1);
                read();
            }
        }
    //проверка контрольной суммы
    if (msg[9]==calcCS(msg))
        emit sendMsgToHandler(msg);
    msg.clear();
}

void PortUdp::send(QByteArray msg)
{
    qDebug() << "udp write";
    int numSet;
    if ((msg[3] & 2)>0)
        numSet = 1;
    else numSet = 2;
    switch (numSet) {
    case 1:
        socketDr1->write(msg);
        break;
    case 2:
        socketDr2->write(msg);
    default:
        break;
    }
}
