#include <mainwindow.h>
#include <ui_mainwindow.h>
#include <QDebug>

void MainWindow::sendQueue()
{
    if (queueData.size()==0)
        return;
    QByteArray msg(10, 0);
    qCopy(queueData.begin(), queueData.begin()+10, msg.begin());
    queueData.remove(0, 10);
    qDebug() << queueData.toHex();
    send(msg);
    if ((msg[3]&0xE0)==0x80){
        if ((msg[3]&2)==2){
            signAnsDR1 = false;
            waitTimerDR1->start(100);
        }
        else{
            signAnsDR2 = false;
            waitTimerDR2->start(100);
        }
    }
    if (removedData.size()>0){
        emit sendToConsoleRead(removedData);
        removedData.clear();
    }
}

void MainWindow::rememberRemovedData(QByteArray data)
{
    qDebug() << "remember slot";
    removedData.append(data);
}

void MainWindow::readMsg(short ku, QByteArray msg)
{
    emit sendToConsoleRead(msg);
    //если приходит сообщение установки параметров, устанавливаю ЧКК
    if (ku==0)
        setChKK(msg);
    //дальше обрабатываю только сообщение ТС
    if (ku!=4)
        return;
    if ((msg[3]&2)==2)
        showDR1(msg);
    else showDR2(msg);
}


void MainWindow::showDR1(QByteArray msg)
{
    signAnsDR1 = true;
    bool powPRD = false, KSV = false, state = false, ZD = false, radiation = false;
    //ЧКК
    ui->ChKK1->setValue((msg[4] & 126) >> 1);
    //исправность комплекта
    //норма мощности
    if ((msg[7]&8)==8)
        powPRD = true;
    //норма КСВ
    if ((msg[7]&16)==16)
        KSV = true;
    if ((msg[7]&32)==32)
        state = true;
    if ((msg[5]&128)==128)
        ZD = true;
    if ((msg[3]&8)==8)
        radiation = true;
    setStyleDR1(powPRD, KSV, state, radiation, ZD);
}

void MainWindow::showDR2(QByteArray msg)
{
    signAnsDR2 = true;
    bool powPRD = false, KSV = false, state = false, ZD = false, radiation = false;
    //ЧКК
    ui->ChKK2->setValue((msg[4] & 126) >> 1);
    //исправность комплекта
    //норма мощности
    if ((msg[7]&8)==8)
        powPRD = true;
    //норма КСВ
    if ((msg[7]&16)==16)
        KSV = true;
    if ((msg[7]&32)==32)
        state = true;
    if ((msg[3]&8)==8)
        radiation = true;
    if ((msg[5]&128)==128)
        ZD = true;
    setStyleDR2(powPRD, KSV, state, radiation, ZD);
}

void MainWindow::setStyleDR1(bool powPRD, bool KSV, bool state, bool radiation, bool ZD)
{
    //норма комплекта
    if ((powPRD==true) and (KSV==true) and (state==true) and (ZD==true)){
        ui->K1norm->setStyleSheet(green);
        ui->K1avar->setStyleSheet(gray);
        ui->K1con->setStyleSheet(gray);
    }
    else{
        amountAvarK1++;
        if ((amountAvarK1%7)==0){
            ui->K1norm->setStyleSheet(gray);
            ui->K1avar->setStyleSheet(red);
            ui->K1con->setStyleSheet(gray);
            ui->K1amountAvar->setValue(amountAvarK1/7);
        }
    }
    //исправность ПРД, КСВ
    if (radiation==true){
        if (KSV==true)
            ui->K1_KSV->setStyleSheet(green);
        else if ((amountAvarK1%7)==0) ui->K1_KSV->setStyleSheet(red);
        if (powPRD==true)
            ui->K1_P->setStyleSheet(green);
        else if ((amountAvarK1%7)==0) ui->K1_P->setStyleSheet(red);
        ui->K1rad->setStyleSheet(green);
    }
    else{
        ui->K1_P->setStyleSheet(gray);
        ui->K1_KSV->setStyleSheet(gray);
        ui->K1rad->setStyleSheet(gray);
    }
}

void MainWindow::setStyleDR2(bool powPRD, bool KSV, bool state, bool radiation, bool ZD)
{
    //норма комплекта
    if ((powPRD==true) and (KSV==true) and (state==true) and (ZD==true)){
        ui->K2norm->setStyleSheet(green);
        ui->K2avar->setStyleSheet(gray);
        ui->K2con->setStyleSheet(gray);
    }
    else{
        amountAvarK2++;
        if ((amountAvarK2%7)==0){
            ui->K2norm->setStyleSheet(gray);
            ui->K2avar->setStyleSheet(red);
            ui->K2con->setStyleSheet(gray);
            ui->K2amountAvar->setValue(amountAvarK2/7);
        }
    }
    //исправность ПРД, КСВ
    if (radiation==true){
        if (KSV==true)
            ui->K2_KSV->setStyleSheet(green);
        else if ((amountAvarK2%7)==0) ui->K2_KSV->setStyleSheet(red);
        if (powPRD==true)
            ui->K2_P->setStyleSheet(green);
        else if ((amountAvarK2%7)==0) ui->K2_P->setStyleSheet(red);
        ui->K2rad->setStyleSheet(green);
    }
    else{
        ui->K2_P->setStyleSheet(gray);
        ui->K2_KSV->setStyleSheet(gray);
        ui->K2rad->setStyleSheet(gray);
    }
}

void MainWindow::controlConnectDR1()
{
    if (signAnsDR1==true)
        ui->K1con->setStyleSheet(gray);
    else ui->K1con->setStyleSheet(red);
    waitTimerDR1->stop();
}

void MainWindow::controlConnectDR2()
{
    if (signAnsDR2==true)
        ui->K2con->setStyleSheet(gray);
    else ui->K2con->setStyleSheet(red);
    waitTimerDR2->stop();
}

void MainWindow::setChKK(QByteArray msg)
{
    if ((msg[3]&2)==2){//ЧКК1
        ui->ChKK1->setValue((msg[4] & 126) >> 1);
    }
    else{//ЧКК2
        ui->ChKK2->setValue((msg[4] & 126) >> 1);
    }
}
