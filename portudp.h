﻿#ifndef PORTUDP_H
#define PORTUDP_H

#include <QObject>
#include <QUdpSocket>
#include <QSettings>
#include <functions.h>
#include <QApplication>

class PortUdp : public QObject
{
    Q_OBJECT
public:
    explicit PortUdp(QObject *parent = 0);
    ~PortUdp();

    void init();

private slots:
    void read();
    void send(QByteArray);

signals:
    void readMsg(QByteArray);
    void sendMsgToHandler(QByteArray);
    void sendRemovedData(QByteArray);

public slots:
    void open();
    void close();
    bool checkSockets();

private:
    const QString appPath;
    QSettings udpSettings;
    QUdpSocket *socketDr1, *socketDr2, *socketKSRP;
    int dr1_port, dr2_port, ksrp_port;
    QString dr1_ip, dr2_ip, ksrp_ip;
    QByteArray recMas, msg;
};

#endif // PORTUDP_H
