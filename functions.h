﻿#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QBitArray>
#include <QByteArray>
#include <QVector>
#include <cmath>
#include <stdint.h>

    extern QBitArray bytesToBits(const QByteArray &bytes);
    extern quint8 checkSum(QByteArray &);
    extern void formCheckMas(QVector <bool> *, QByteArray);
    extern uint8_t csAddBit(uint8_t, uint8_t);
    extern uint8_t calcCS(const QByteArray &);


#endif // FUNCTIONS_H
