﻿#ifndef MESSAGEHANDLER_H
#define MESSAGEHANDLER_H

#include <QObject>
#include <QDebug>
#include <functions.h>

class MessageHandler: public QObject
{
    Q_OBJECT
public:
    explicit MessageHandler(QObject *parent = 0);
    ~MessageHandler();

private:
    short int msgKU;
    bool checkMsg(QByteArray &data);
    void getMsgKU(QByteArray ku);
    void reset();

signals:
    void signMsg(short int, QByteArray);

private slots:
    void getMsg(QByteArray);

};

#endif // MESSAGEHANDLER_H
