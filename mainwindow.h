#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <messagehandler.h>
#include <port_rs485.h>
#include <console.h>
#include <portudp.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void signSendToRs485(QByteArray);
    void signSendToUDP(QByteArray);
    void sendToConsoleWrite(QByteArray);
    void sendToConsoleRead(QByteArray);

public slots:
    void readMsg(short int, QByteArray);

private slots:
    void on_pbBeginControl_clicked();
    void on_pbEndControl_clicked();
    void endForm();
    void on_RadOff_clicked();
    void on_K1on_clicked();
    void sendQueue();
    void send(QByteArray);
    void beginRequest();
    void requestTechState();
    void showDR1(QByteArray);
    void showDR2(QByteArray);
    void setStyleDR1(bool, bool, bool, bool, bool);
    void setStyleDR2(bool, bool, bool, bool, bool);
    void on_K2on_clicked();
    void on_ChKKButton_clicked();
    void controlConnectDR1();
    void controlConnectDR2();
    void rememberRemovedData(QByteArray);
    void choicePort();
    void setChKK(QByteArray);

private:
    Ui::MainWindow *ui;
    QTimer *queueTimer, *reqTimer, *waitTimerDR1, *waitTimerDR2, *begReqTimer, *endTimer;
    QString green, gray, red;
    int amountAvarK1, amountAvarK2;
    QByteArray queueData, removedData;
    MessageHandler *myMH;
    Port_RS485 *serialPort;
    PortUdp *udpSockets;
    bool signAnsDR1, signAnsDR2;
    Console *console;
};

#endif // MAINWINDOW_H
